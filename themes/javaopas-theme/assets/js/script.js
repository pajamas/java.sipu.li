const cursorCSS = "* { cursor: url(\"/images/yellowcursor.png\"), auto !important }"

function getCookie(c_name) {
    if (typeof localStorage != "undefined") {
        return localStorage.getItem(c_name);
    }
    else {
        return "undefined";
    }
}

function setCookie(c_name, value, expiredays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    if (typeof localStorage != "undefined") {
        localStorage.setItem(c_name, value);
    }
}

function checkboxClick() {
    let checkBox = document.getElementById("check");
    if (checkBox.checked) {
        setCookie("cursoreffects","on",365)
        window.addEventListener("mousemove", doCursorEffects)
    } else {
        setCookie("cursoreffects","off",365)
        window.removeEventListener("mousemove", doCursorEffects)
    }
}

themeLink = (theme) => {
    return "<link id=\"themelink\" rel=\"stylesheet\" type=\"text/css\" href=\"/css/" + theme + "theme.css?v=" + Math.random().toString(36).substring(2, 15) + "\">"
}

toggleTheme = (theme) => {
    let checkBox = document.getElementById(theme + "themetogglecheck")
    if (checkBox.checked) {
        setCookie("darktheme","on",1000)
        setDarkTheme(true)
    } else {
        setCookie("darktheme","off",1000)
        setDarkTheme(false)
    }
}

function setDarkTheme(on) {
    let html = document.getElementsByTagName("html")[0]
    html.dataset.theme = on ? "dark" : ""
}

cursorToggle = () => {
    let checkBox = document.getElementById("cursortogglecheck")
    if (checkBox.checked) {
        setCookie("customcursor","on",365)
        cursorStyle = document.getElementById("cursorstyle")
        cursorStyle.innerHTML = cursorCSS
    } else {
        setCookie("customcursor","off",365)
        cursorStyle = document.getElementById("cursorstyle")
        cursorStyle.innerHTML = "* { cursor: auto }"
    }
}

function onLoad() {
    let mobile = false
    // mobile?
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        mobile = true
        nonMobile = document.querySelector('.non-mobile')
        nonMobile.style.display = "none"
    }

    cookie = getCookie("cursoreffects")
    // localstorage not available?
    if (cookie === "undefined") {
        return
    }
    // dark theme?
    cookie3 = getCookie("darktheme")
    if (cookie3 === "on") {
        if (window.location.pathname === "/asetukset/") {
            let checkBox = document.getElementById("darkthemetogglecheck")
            checkBox.checked = true
        }
    } else if (cookie3 == null) {
        if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
            if (window.location.pathname === "/asetukset/") {
                let checkBox = document.getElementById("darkthemetogglecheck")
                checkBox.checked = true
            }
            setDarkTheme(true)
        }
    }

    if (!mobile) {
        let switchOn = true
        if (cookie === "off") {
            switchOn = false
        }
        // create switch
        let switchdiv = document.createElement("div")
        switchdiv.className = "switchdiv"
        switchdiv.innerHTML = "<div class=\"falling-particle-img\"></div><label class=\"switch\"><input type=\"checkbox\" id=\"check\" onclick=\"checkboxClick()\"><span class=\"slider round\"></span></label>"
        switchdivcontainer = document.getElementById("switchdiv-container")
        switchdivcontainer.appendChild(switchdiv)
        
        if (switchOn) {
            let checkbox = document.getElementById("check")
            checkbox.checked = true
            window.addEventListener("mousemove", doCursorEffects)
        }

        // if custom cursor on, do custom cursor

        cookie2 = getCookie("customcursor")
        if (cookie2 !== "off") {

        if (window.location.pathname === "/asetukset/") {
            let checkBox = document.getElementById("cursortogglecheck")
            checkBox.checked = true
        }

        cursorStyle = document.getElementById("cursorstyle")
        cursorStyle.innerHTML = cursorCSS
        }
    }

    
}

function doCursorEffects(e) {
	var to_append = document.getElementsByClassName('loader-container')[0];
	var all = document.getElementsByClassName('loader-container');

	var parent_div = document.createElement('div');
	parent_div.className = "loader-container";
	var inner_div = document.createElement('div');
	inner_div.className = "loader";
	parent_div.appendChild(inner_div)
	var d = document.body.appendChild(parent_div);

    var xRand;
    var yRand;
    // Returns a random integer from 0 to 9:
    xRand = Math.floor(Math.random() * 10);
    yRand = Math.floor(Math.random() * 10);
	parent_div.style.left = (e.clientX - 15 + xRand)+'px';
	parent_div.style.top = (window.scrollY + e.clientY - 15 + yRand)+'px';
    var choice = Math.floor(Math.random() * 3);
    if (document.getElementsByClassName('loader-container').length > 50) {
		document.body.removeChild(to_append)
	}
    if (choice == 0) {
        inner_div.style.animationName = "downleft";
    } else if (choice == 1) {
        inner_div.style.animationName = "down";
    } else {
        inner_div.style.animationName = "downright";
    }
}

cookie3 = getCookie("darktheme")
    if (cookie3 === "on") {
        let html = document.getElementsByTagName("html")[0]
        html.dataset.theme = "dark"
    } else if (cookie3 == null) {
        if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
        let html = document.getElementsByTagName("html")[0]
        html.dataset.theme = "dark"
        }
    }
window.addEventListener("load", function () {
    onLoad();
});

solutionClick = (solutionNum) => {
    solutionId = "solution" + solutionNum
    solutionDiv = document.getElementById(solutionId)

    solutionHidden = solutionDiv.querySelector('.solution-hidden')
    solutionButton = solutionDiv.querySelector('button')
    if (solutionHidden.style.display == "block") {
        solutionHidden.style.display = "none"
        solutionButton.innerHTML = "NÄYTÄ"
    } else {
        solutionHidden.style.display = "block"
        solutionButton.innerHTML = "PIILOTA"
        solutionDiv.scrollIntoView({ behavior: 'smooth' })
    }

};

exerciseClick = (exerciseNum) => {
    exerciseId = "exercise" + exerciseNum
    exerciseDiv = document.getElementById(exerciseId)

    exerciseHidden = exerciseDiv.querySelector('.exercise-hidden')
    plus = exerciseDiv.querySelector('.exercise span')
    if (exerciseHidden.style.display == "block") {
        exerciseHidden.style.display = "none"
        plus.innerHTML = "+"
    } else {
        exerciseHidden.style.display = "block"
        plus.innerHTML = "-"
        exerciseDiv.scrollIntoView({ behavior: 'smooth' })
    }
};

---
title: Javan perusjutut
---

### Ohjelman rakenne

Java-ohjelma muodostetaan näin:

```java
public class Ohjelma {
    public static void main(String[] args) {
        // koodi tänne
    }
    // muut funktiot tänne
}
```

Tiedoston nimen täytyy olla sama kuin julkisen luokan nimi, eli tässä tapauksessa
`Ohjelma.java`.

Ohjelman päämetodin nimi on `main`.

### Tulostaminen

Javassa tulostetaan System.out.println()-komennolla:

```java
System.out.println("Moi!");
```

Tämä komento lisää tekstin jälkeen rivinvaihdon. Jos et halua rivinvaihtoa, käytä
System.out.print()-komentoa.

```java
System.out.println("1");
System.out.print("2");
System.out.print("3");
System.out.println("4");
System.out.println("5");
```

Tämä tulostaa:
```java
1
234
5
```

### Tyypit

Java on vahvasti tyypitetty kieli, mikä tarkoittaa sitä, että muuttujien ja funktioiden
datatyypit täytyy ilmoittaa itse, toisin kuin Pythonissa, jossa tyyppejä ei tarvitse ilmoittaa.

Javan tärkeimmät datatyypit ovat:

- int (kokonaisluku)
- double (liukuluku)
- char (merkki)
- String (merkkijono)
- boolean (totuusarvo)

Muuttujan tyyppi ilmoitetaan tähän tyyliin:

```java
int numero = 5;
// luotu kokonaislukutyyppinen muuttuja nimeltä numero
// nyt muuttujaa voi käyttää ilmoittamatta enää tyyppiä
System.out.println(numero + 4);
```

Koodi tulostaa:

```
9
```

Funktion tyyppi ilmoitetaan näin:

```java
// huomaa, että myös parametrien tyypit ilmoitetaan
int laskeYhteen(int numero1, int numero2) {
    return numero1 + numero2;
}
```

Funktion tyyppi tarkoittaa siis funktion palauttaman arvon tyyppiä. Sama funktio ei siis
voi palauttaa joskus kokonaislukua ja joskus merkkijonoa, vaan tyypin täytyy olla aina sama.

Jos funktion ei haluta palauttavan mitään, sen tyypiksi laitetaan `void`. Jos funktion tyyppi
ei ole void, niin sen täytyy aina palauttaa jotain.

### Syötteen pyytäminen käyttäjältä

Syötteen saamiseen käytetään Scanner-oliota. Ensin se täytyy importata (importit laitetaan
tiedoston alkuun ennen luokan määrittelyä):

```java
import java.util.Scanner;
```

Uusi Scanner-olio luodaan näin:

```java
Scanner lukija = new Scanner(System.in);
```
 
Tämä koodi siis määrittelee muuttujan, jonka tyyppi on Scanner ja nimi on lukija, ja sitten
luo uuden Scanner-olion joka tallennetaan tähän muuttujaan. `System.in` tarkoittaa 
komentoriviltä saatua syötettä. Uusi Scanner siis ottaa argumentiksi syötteen lähteen.

Nyt kun Scanner-olio on luotu, sitä voi käyttää näin:

```java
String syote = lukija.nextLine();
```

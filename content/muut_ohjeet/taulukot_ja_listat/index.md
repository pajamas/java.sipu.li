---
title: Taulukot ja listat
---

### Arrayn muunnos ArrayListiksi

```java
String[] taulukko = {"abc","def","ghi"};
ArrayList<String> lista = new ArrayList<>(Arrays.asList(taulukko));
```

---
title: Virheviestien tulkitseminen
---

### `cannot find symbol`

Tarkastellaan tätä koodia:

```java
public class Testi {
    public static void main(String[] args) {
        System.out.println(abc);
    }
}
```

Java-kääntäjä huomauttaa virheestä:

```
Testi.java:3: error: cannot find symbol
        System.out.println(abc);
                           ^
  symbol:   variable abc
  location: class Testi
1 error
```

"Cannot find symbol" tarkoittaa sitä, että koodissasi käytetään määrittelemätöntä muuttujaa
tai metodia. Se muuttuja tai funktio, jota ei ole määritelty, lukee rivillä
`symbol: variable abc`. Tästä näemme, että muuttujaa `abc` ei löydy. Korjataan koodi:

```java
public class Testi {
    public static void main(String[] args) {
        String abc = "määritelty muuttuja";
        System.out.println(abc);     
    }
}
```

Nyt koodi toimii ja tulostaa "määritelty muuttuja".
